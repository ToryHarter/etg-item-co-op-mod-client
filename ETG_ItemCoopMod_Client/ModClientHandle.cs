﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ETG_ItemCoopMod_Client
{
    public class ModClientHandle : MonoBehaviour
    {
        public static void Welcome(Packet packet)
        {
            string msg = packet.ReadString();
            int id = packet.ReadInt();

            ETGModConsole.Log($"<color=#FFD700>{msg}</color>");
            ModClient.Instance.Id = id;
            if (String.IsNullOrEmpty(ModClient.Instance.Username))
            {
                ModClient.Instance.Username = $"Player {id}";
            }
            ClientSend.WelcomeReceived();
        }

        public static void GiveItem(Packet packet)
        {
            int fromClientId = packet.ReadInt();
            string fromUsername = packet.ReadString();
            int itemId = packet.ReadInt();
            string itemName = packet.ReadString();
            ItemType itemType = (ItemType)packet.ReadInt();

            if (!ETG_ItemCoopMod_Client.CharacterStarterItems.Concat(ETG_ItemCoopMod_Client.CharacterStarterItems).Contains(itemId))
            {
                ETGModConsole.Log($"<color=#FFD700>{fromUsername} gave you an item: {itemName}</color>");
                ETG_ItemCoopMod_Client.ItemsPickedUpThisRun.Add(itemId);
                ETG_ItemCoopMod_Client.GiveItem(itemId, itemType);
            }
        }

        public static void PlayerInfoRefresh(Packet packet)
        {
            var serializedPlayerInfo = packet.ReadString();

            List<PlayerInfo> playerInfos = JsonConvert.DeserializeObject<List<PlayerInfo>>(serializedPlayerInfo);
            ETG_ItemCoopMod_Client.PlayerInfos = playerInfos;

            ETG_ItemCoopMod_Client.RefreshPlayerInfo();
        }
    }
}
