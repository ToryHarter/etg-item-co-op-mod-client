﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETG_ItemCoopMod_Client
{
    public enum ItemType
    {
        Gun = 0,
        ActiveItem = 1,
        PassiveItem = 2
    }
}
