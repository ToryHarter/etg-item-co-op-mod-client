﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETG_ItemCoopMod_Client
{
    public class PlayerInfo
    {
        public int Id;
        public string Username;
        public string RemoteEndPoint;
    }
}
