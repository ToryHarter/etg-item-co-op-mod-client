﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ETG_ItemCoopMod_Client
{
    public class ClientSend : MonoBehaviour
    {
        private static void SendTCPData(Packet packet)
        {
            packet.WriteLength();
            ModClient.Instance.Tcp.SendData(packet);
        }

        public static void WelcomeReceived()
        {
            using (Packet packet = new Packet((int)ClientPackets.welcomeReceived))
            {
                packet.Write(ModClient.Instance.Id);
                packet.Write(ModClient.Instance.Username);

                SendTCPData(packet);
            }
        }

        public static void GotItem(int itemId, string itemName, ItemType itemType)
        {
            using (Packet packet = new Packet((int)ClientPackets.gotItem))
            {
                packet.Write(ModClient.Instance.Id);
                packet.Write(ModClient.Instance.Username);
                packet.Write(itemId);
                packet.Write(itemName);
                packet.Write((int)itemType);

                SendTCPData(packet);
            }
        }
    }
}
