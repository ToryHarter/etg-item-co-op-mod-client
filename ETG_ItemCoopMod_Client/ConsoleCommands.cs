﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETG_ItemCoopMod_Client
{
    class ConsoleCommands
    {
        public static void AddICOCommands()
        {
            ETGModConsole.Commands.AddGroup("ico");
            ETGModConsole.Commands.GetGroup("ico").AddGroup("help", Help);
            ETGModConsole.Commands.GetGroup("ico").AddGroup("connect", Connect);
            ETGModConsole.Commands.GetGroup("ico").AddGroup("con", Connect);
            ETGModConsole.Commands.GetGroup("ico").AddGroup("reconnect", Reconnect);
            ETGModConsole.Commands.GetGroup("ico").AddGroup("recon", Reconnect);
            ETGModConsole.Commands.GetGroup("ico").AddGroup("disconnect", Disconnect);
            ETGModConsole.Commands.GetGroup("ico").AddGroup("dc", Disconnect);
        }

        // Humbly borrowed from: https://modworkshop.net/mod/23271
        private static void Help(string[] args)
        {
            try
            {
                ETGModConsole.Log("");
                ETGModConsole.Log("<size=100><color=#ff0000ff>Item Co-op</color></size>");
                ETGModConsole.Log("<size=100><color=#ff0000ff>By The_Rot_Bot</color></size>");
                ETGModConsole.Log("<size=100><color=#ff0000ff>--------------------------------</color></size>");
                ETGModConsole.Log("Item Co-op Commands:");
                ETGModConsole.Log("");
                ETGModConsole.Log("<color=#FFFF33>ico help</color> - lists Item Co-op commands");
                ETGModConsole.Log("<color=#FFFF33>ico connect [IPv4 Address] [Username]</color> - connects to the player hosted Item Co-op server at the provided IPv4 address under the given username");
                ETGModConsole.Log("<color=#FFFF33>ico con [IPv4] [Username]</color> - shorthand for the ico connect command");
                ETGModConsole.Log("<color=#FFFF33>ico reconnect</color> - reconnects to the previously connected player hosted Item Co-op server with the then provided username");
                ETGModConsole.Log("<color=#FFFF33>ico recon</color> - shorthand for the ico reconnect command");
                ETGModConsole.Log("<color=#FFFF33>ico disconnect</color> - disconnect from the current player hosted Item Co-op server");
                ETGModConsole.Log("<color=#FFFF33>ico dc</color> - shorthand for the ico disconnect command");
            }
            catch (Exception e)
            {
                ETGModConsole.Log($"Something went wrong showing help: {e.Message}");
                ETGModConsole.Log(e.InnerException.Message);
            }
        }

        private static void Connect(string[] args)
        {
            try
            {
                if (args.Length >= 1)
                {
                    string username = "";
                    if (args.Length >= 2)
                    {
                        username = args[1];
                    }

                    ETGModConsole.Log("Connecting to server...");
                    if (args[0] == "localhost")
                    {
                        ModClient.Instance.ConnectToServer("127.0.0.1", username);
                    }
                    else
                    {
                        ModClient.Instance.ConnectToServer(args[0], username);
                    }
                }
            }
            catch (Exception e)
            {
                ETGModConsole.Log($"Something went wrong connecting: {e.Message}");
                ETGModConsole.Log(e.InnerException.Message);
            }
        }

        private static void Reconnect(string[] args)
        {
            try
            {
                if (!String.IsNullOrEmpty(ModClient.Instance.PreviouslyConnectedIP))
                {
                    Connect(new string[] { ModClient.Instance.PreviouslyConnectedIP, ModClient.Instance.Username });
                }
                else
                {
                    ETGModConsole.Log("No previous connection to reconnect to!");
                }
            }
            catch (Exception e)
            {
                ETGModConsole.Log($"Something went wrong reconnecting: {e.Message}");
                ETGModConsole.Log(e.InnerException.Message);
            }
        }

        private static void Disconnect(string[] args)
        {
            try
            {
                ModClient.Instance.Disconnect();
            }
            catch (Exception e)
            {
                ETGModConsole.Log($"Something went wrong disconnecting: {e.Message}");
                ETGModConsole.Log(e.InnerException.Message);
            }
        }
    }
}
