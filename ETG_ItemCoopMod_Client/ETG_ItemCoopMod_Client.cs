﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoMod.RuntimeDetour;
using System.Reflection;
using HutongGames.PlayMaker.Actions;
using Dungeonator;
using UnityEngine;
using UnityEngine.UI;

namespace ETG_ItemCoopMod_Client
{
    public class ETG_ItemCoopMod_Client : ETGModule
    {
        public static List<int> ItemsPickedUpThisRun;
        public static List<int> CharacterStarterItems;
        public static List<PlayerInfo> PlayerInfos;

        public static VerticalLayoutGroup PlayerInfoGroup;
        public static RectTransform PlayerInfoGroupRectTransform;
        public static int FontSize;

        public override void Init() {}

        public override void Exit() {}

        public override void Start()
        {
            ItemsPickedUpThisRun = new List<int>();
            CharacterStarterItems = new List<int>();
            PlayerInfos = new List<PlayerInfo>();
            FontSize = 42;

            this.SetupTcp();
            this.SetupHooks();
            this.SetupUI();
            ConsoleCommands.AddICOCommands();
        }

        private void SetupTcp()
        {
            ETGModMainBehaviour.Instance.gameObject.AddComponent<ModClient>();
            ETGModMainBehaviour.Instance.gameObject.AddComponent<ThreadManager>();
            ETGModMainBehaviour.Instance.gameObject.AddComponent<ModClientHandle>();
        }

        private void SetupHooks()
        {
            new Hook(
                typeof(PlayerController).GetMethod("InitializeInventory", BindingFlags.NonPublic | BindingFlags.Instance),
                typeof(ETG_ItemCoopMod_Client).GetMethod("Hook_PlayerController_InitializeInventory", BindingFlags.NonPublic | BindingFlags.Instance),
                this);

            new Hook(
                typeof(Gun).GetMethod("Pickup", BindingFlags.Public | BindingFlags.Instance),
                typeof(ETG_ItemCoopMod_Client).GetMethod("Hook_Gun_Pickup", BindingFlags.Public | BindingFlags.Instance),
                this);

            new Hook(
                typeof(PassiveItem).GetMethod("Pickup", BindingFlags.Public | BindingFlags.Instance),
                typeof(ETG_ItemCoopMod_Client).GetMethod("Hook_PassiveItem_Pickup", BindingFlags.Public | BindingFlags.Instance),
                this);

            new Hook(
                typeof(PlayerItem).GetMethod("Pickup", BindingFlags.Public | BindingFlags.Instance),
                typeof(ETG_ItemCoopMod_Client).GetMethod("Hook_PlayerItem_Pickup", BindingFlags.Public | BindingFlags.Instance),
                this);

            new Hook(
                typeof(LevelNameUIManager).GetMethod("ShowLevelName", BindingFlags.Public | BindingFlags.Instance),
                typeof(ETG_ItemCoopMod_Client).GetMethod("Hook_LevelNameUIManager_ShowLevelName", BindingFlags.Public | BindingFlags.Instance),
                this);

            new Hook(
                typeof(FoyerCharacterSelectFlag).GetMethod("OnSelectedCharacterCallback", BindingFlags.Public | BindingFlags.Instance),
                typeof(ETG_ItemCoopMod_Client).GetMethod("Hook_FoyerCharacterSelectFlag_OnSelectedCharacterCallback", BindingFlags.Public | BindingFlags.Instance),
                this);
        }

        private void SetupUI()
        {
            GUI.Init();

            GameObject thing = new GameObject();
            PlayerInfoGroupRectTransform = thing.AddComponent<RectTransform>();
            PlayerInfoGroupRectTransform.SetTextAnchor(TextAnchor.MiddleLeft);
            GUI.UpdateUIOffset();

            PlayerInfoGroup = thing.AddComponent<VerticalLayoutGroup>();
            PlayerInfoGroup.transform.parent = GUI.m_canvas.transform;
            PlayerInfoGroup.enabled = true;
            
            var textObject = GUI.CreateText(null, new Vector2(0f, 0f), "Offline", TextAnchor.MiddleLeft, ETG_ItemCoopMod_Client.FontSize);
            textObject.text = "Offline";
            textObject.transform.parent = ETG_ItemCoopMod_Client.PlayerInfoGroup.transform;
        }

        public void RegisterGotItem(int itemId, string itemName, ItemType itemType)
        {
            if (!ItemsPickedUpThisRun.Concat(CharacterStarterItems).Contains(itemId))
            {
                ItemsPickedUpThisRun.Add(itemId);
                if (ModClient.Instance.IsConnected)
                {
                    ClientSend.GotItem(itemId, itemName, itemType);
                }
            }
        }

        public static void GiveItem(int itemId, ItemType itemType)
        {
            var playerController = Resources.FindObjectsOfTypeAll(typeof(PlayerController)).FirstOrDefault() as PlayerController;

            switch (itemType)
            {
                case ItemType.Gun:
                    Gun gun = PickupObjectDatabase.GetById(itemId) as Gun;
                    gun.Pickup(playerController);
                    break;
                case ItemType.ActiveItem:
                    PlayerItem playerItem = PickupObjectDatabase.GetById(itemId) as PlayerItem;
                    playerItem.Pickup(playerController);
                    break;
                case ItemType.PassiveItem:
                    var passiveItem = PickupObjectDatabase.GetById(itemId) as PassiveItem;
                    passiveItem.Pickup(playerController);
                    break;
            }
        }

        public static void RefreshPlayerInfo()
        {
            foreach (Transform child in ETG_ItemCoopMod_Client.PlayerInfoGroup.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            ETGModConsole.Log("");
            ETGModConsole.Log("Player List");
            ETGModConsole.Log("-----------");
            foreach (var playerInfo in ETG_ItemCoopMod_Client.PlayerInfos)
            {
                ETGModConsole.Log($"{playerInfo.Username}");
                var textObject = GUI.CreateText(null, new Vector2(0f, 0f), playerInfo.Username, TextAnchor.MiddleLeft, ETG_ItemCoopMod_Client.FontSize);
                textObject.text = playerInfo.Username;
                textObject.transform.parent = ETG_ItemCoopMod_Client.PlayerInfoGroup.transform;
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(ETG_ItemCoopMod_Client.PlayerInfoGroup.transform as RectTransform);
        }

        private void Hook_PlayerController_InitializeInventory(Action<PlayerController> orig, PlayerController self)
        {
            CharacterStarterItems.Clear();
            if (self.UsingAlternateStartingGuns)
            {
                CharacterStarterItems.AddRange(self.startingAlternateGunIds);
            }
            else
            {
                CharacterStarterItems.AddRange(self.startingGunIds);
            }
            CharacterStarterItems.AddRange(self.startingActiveItemIds);
            CharacterStarterItems.AddRange(self.startingPassiveItemIds);

            orig(self);
        }

        public void Hook_Gun_Pickup(Action<Gun, PlayerController> orig, Gun self, PlayerController player)
        {
            if (GameManager.Instance.CurrentLevelOverrideState != GameManager.LevelOverrideState.FOYER)
            {
                this.RegisterGotItem(self.PickupObjectId, self.DisplayName, ItemType.Gun);
            }

            orig(self, player);
        }

        public void Hook_PassiveItem_Pickup(Action<PassiveItem, PlayerController> orig, PassiveItem self, PlayerController player)
        {
            if (GameManager.Instance.CurrentLevelOverrideState != GameManager.LevelOverrideState.FOYER)
            {
                this.RegisterGotItem(self.PickupObjectId, self.DisplayName, ItemType.PassiveItem);
            }

            orig(self, player);
        }

        public void Hook_PlayerItem_Pickup(Action<PlayerItem, PlayerController> orig, PlayerItem self, PlayerController player)
        {
            if (GameManager.Instance.CurrentLevelOverrideState != GameManager.LevelOverrideState.FOYER)
            {
                this.RegisterGotItem(self.PickupObjectId, self.DisplayName, ItemType.ActiveItem);
            }

            orig(self, player);
        }

        public void Hook_LevelNameUIManager_ShowLevelName(Action<LevelNameUIManager, Dungeon> orig, LevelNameUIManager self, Dungeon d)
        {
            // New Run
            if (GameManager.Instance.Dungeon.tileIndices.tilesetId == GlobalDungeonData.ValidTilesets.CASTLEGEON)
            {
                ItemsPickedUpThisRun.Clear();
                ItemsPickedUpThisRun.AddRange(CharacterStarterItems);
                CharacterStarterItems.Clear();
            }

            orig(self, d);
        }

        public void Hook_FoyerCharacterSelectFlag_OnSelectedCharacterCallback(Action<FoyerCharacterSelectFlag, PlayerController> orig, FoyerCharacterSelectFlag self, PlayerController newCharacter)
        {
            orig(self, newCharacter);

            //ETGModConsole.Log("Character Selected");
        }
    }
}
